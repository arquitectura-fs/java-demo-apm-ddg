package com.javasimpleddg.javamysql;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

import java.io.BufferedReader;
/*Bloque para importar clases necesarias*/
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

//se deben agregar estas   librerias provenientes del repositorio de Maven

import io.opentracing.Tracer;
import io.opentracing.util.GlobalTracer;
import io.opentracing.Scope;
import io.opentracing.Span;
import datadog.trace.api.DDTags;
//import io.opentracing.tag.Tags;
//import datadog.trace.api.Trace;
//import datadog.opentracing.DDTracer;

//de la documentacion ubicada en : https://docs.datadoghq.com/tracing/setup_overview/open_standards/java/
//y en https://docs.datadoghq.com/tracing/setup_overview/custom_instrumentation/java/#trace-annotations

public class demo_APM_DDg {

	private static Timer timer;
	private final Logger logger = Logger.getLogger(demo_APM_DDg.class.getName());
	public static final String DRIVER = "com.mysql.jdbc.Driver";
	public static final String URL = "jdbc:mysql://127.0.0.1:3306/mydb?autoReconnect=true&useSSL=false";
	public static final String USER = "student";
	public static final String PASSWORD = "pass123";

	// Objeto de conexi�n a base de datos tipo Connection
	static Connection conn = null;
	// Resultset donde vamos a buscar el resultado que genera el query
	static ResultSet rs = null;
	// Abrimos conexi�n a base de datos
	static PreparedStatement ps = null;

	public demo_APM_DDg() {

		try {
			// conexion a DB MySql
			conn = DriverManager.getConnection(demo_APM_DDg.URL, demo_APM_DDg.USER, demo_APM_DDg.PASSWORD);
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			logger.log(Level.SEVERE, e.getMessage(), e);

		}

	}

	public static void main(String[] args) {

		demo_APM_DDg obj = new demo_APM_DDg();
		obj.empezar();
		// obj.setVisible(true);
		timer.start();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("presione Ctrl+C para salir");

		try {
			// Ya tenemos el "buffer", dejando el programa en espera hasta presionar en la
			// terminal las teclas Ctrl+C
			String nombre = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void empezar() {

		timer = new Timer(1000, new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Tracer tracer = GlobalTracer.get();
				String inputString = "", nombreCliente[] = { "JOSE GOMEZ", "JUAN MARTINEZ", "ANA VIDAL" };
				int contCliente;
				// Tags can be set when creating the span
				Span span = tracer.buildSpan("conexion.database").withTag(DDTags.SERVICE_NAME, "java app sillas")
						.withTag(DDTags.RESOURCE_NAME, "demo_APM_DDg.database").start();
				try (Scope scope = tracer.activateSpan(span)) {
					// Tags can also be set after creation
					span.setTag("my.tag", "1-2-3");

					// The code you�re tracing
					contCliente = (int) (Math.random() * 3);

					inputString = String.valueOf(carritoComprasSillas(nombreCliente[contCliente]));

					if (inputString.compareTo("-1") != 0) {

						String query = "insert into compra(cant_sillas,userName) values " + "(" + inputString + ",'"
								+ nombreCliente[contCliente] + "')";
						consultaEjecutada(query);
						try {
							ps = conn.prepareStatement(query);
							ps.executeUpdate();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					// end code you�re tracing
				} catch (Exception e1) {
					// Set error on span
					e1.printStackTrace();
				} finally {
					// Close span in a finally block
					span.finish();
				}
			}
		});

	}

	public static int carritoComprasSillas(String cliente) {

		Tracer tracer = GlobalTracer.get();
		int totalSillasIngresadas = 0;
		// Tags can be set when creating the span
		Span span = tracer.buildSpan("carritoCompras.carritoComprasSillas")
				.withTag(DDTags.SERVICE_NAME, "java app - carritoComprasSillas")
				.withTag(DDTags.RESOURCE_NAME, "carritoComprasSillas").start();
		try (Scope scope = tracer.activateSpan(span)) {
			// Tags can also be set after creation
			span.setTag("my.tag", "4-5-6");

			// The code you�re tracing
			totalSillasIngresadas = (int) (Math.floor(Math.random() * 100));
			System.out.println("cantidad de sillas en el carrito : " + totalSillasIngresadas);

			// esto es para crear un sub-span en DDg, basado en una condicion aleatoria
			// tonta
			if ((int) (Math.random() * 2) == 1) {
				compraRealizada(cliente, totalSillasIngresadas);
			} else {
				comprasNoRealizadas(cliente, totalSillasIngresadas);
				// el cliente no compro nada, por tanto no se almacena en la BD
				return -1;
			}

			// end code you�re tracing
		} catch (Exception e) {
			// Set error on span
		} finally {
			// Close span in a finally block
			span.finish();
		}
		return totalSillasIngresadas;
	}

	public static void compraRealizada(String cliente, int totalSillas) {
		Tracer tracer = GlobalTracer.get();
		// Tags can be set when creating the span
		Span span = tracer.buildSpan("compras.compraRealizada")
				.withTag(DDTags.SERVICE_NAME, "java app - compraRealizada")
				.withTag(DDTags.RESOURCE_NAME, "compraRealizada").start();
		try (Scope scope = tracer.activateSpan(span)) {
			// Tags can also be set after creation
			span.setTag("my.tag", "7-8-9");

			// The code you�re tracing
			System.out.println(cliente + " ha Comprado: " + totalSillas + " Sillas...");

			// end code you�re tracing
		} catch (Exception e) {
			// Set error on span
		} finally {
			// Close span in a finally block
			span.finish();
		}

	}

	public static void comprasNoRealizadas(String cliente, int totalSillas) {
		Tracer tracer = GlobalTracer.get();
		// Tags can be set when creating the span
		Span span = tracer.buildSpan("compras.comprasNoRealizadas")
				.withTag(DDTags.SERVICE_NAME, "java app - comprasNoRealizadas")
				.withTag(DDTags.RESOURCE_NAME, "comprasNoRealizadas").start();
		try (Scope scope = tracer.activateSpan(span)) {
			// Tags can also be set after creation
			span.setTag("my.tag", "10-11-12");

			// The code you�re tracing
			System.out.println(cliente + " ha Cancelado el pedido de  " + totalSillas + " Sillas...");

			// end code you�re tracing
		} catch (Exception e) {
			// Set error on span
		} finally {
			// Close span in a finally block
			span.finish();
		}

	}

	public static void consultaEjecutada(String query) {
		Tracer tracer = GlobalTracer.get();
		// Tags can be set when creating the span
		Span span = tracer.buildSpan("consulta.consultaEjecutada")
				.withTag(DDTags.SERVICE_NAME, "java app - consultaEjecutada")
				.withTag(DDTags.RESOURCE_NAME, "consultaEjecutada").start();
		try (Scope scope = tracer.activateSpan(span)) {
			// Tags can also be set after creation
			span.setTag("my.tag", "13-14-15");

			// The code you�re tracing
			System.out.println("query realizado: " + query);

			// end code you�re tracing
		} catch (Exception e) {
			// Set error on span
		} finally {
			// Close span in a finally block
			span.finish();
		}

	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}